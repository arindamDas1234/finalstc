import  React, { Component } from 'react';

import { StyleSheet, View, TouchableOpacity, Text, Dimensions ,TextInput, AsyncStorage, Alert} from 'react-native';
import { NetworkInfo } from "react-native-network-info";
import NetInfo from "@react-native-community/netinfo";
import Icon from 'react-native-vector-icons/Ionicons';
import {URL} from '../api.js';

export default class PasswordUpdate extends Component{
    constructor(props){
        super(props);

        this.state = {
            user_id:"",
            device_id:"",
            modify_by_ip:"",
            newPassword:"",
            oldPasssword:"",
            ip_adress:"",
            otp_field:"",
            is_enterNumber:"",
            newOtp:""
        }
    }

    componentDidMount(){
       
        this.getDevice_id();
        this.getUser_id();
        this.getIpAddress();
    }

    getDevice_id = ()=>{
        AsyncStorage.getItem("device_id")
        .then(result =>{
            this.setState({
                device_id:result
            })
        }).catch(error =>{
            console.log(error);
        })
    }
     
    getUser_id = ()=>{
        AsyncStorage.getItem("user_id")
        .then(result =>{
            this.setState({
                user_id:result
            })
        }).catch(error =>{
            console.log(error);
        })
    }

    getIpAddress = ()=>{
        NetworkInfo.getIPAddress().then(ipAddress =>{
       
            this.setState({
                ip_address:ipAddress.toString()
            })
        });
    }

   

    updatePassword = ()=>{

        if(this.state.newPassword == "" || this.state.oldPassword){
            Alert.alert(
                "Validation",
                "Please enter your desired password in both the fields and then press Update Password"
            )
        }else{
            NetInfo.fetch().then(state =>{
                if(state.isConnected){
                    fetch(URL+"/update_password_by_user_id",{
                        headers:{
                            "Content-Type":"application/x-www-form-urlencoded"
                        },
                        method:"POST",
                        body:"new_password=" +this.state.newPassword+ "&old_password=" +this.state.oldPasssword+
                            "&device_id=" +this.state.device_id+ "&modify_by_id=" +this.state.user_id+ "&modify_by_ip="+this.state.ip_address+ "&user_id="+this.state.user_id
                    }).then(response => response.json())
                    .then(result =>{
                        if(result){
                            if(result.error == false){
                                Alert.alert(
                                    " Success",
                                    "User Password changed successfully"
                                );
                                this.props.navigation.navigate("login")
                            }else{
                                Alert.alert (
                                    "Error",
                                    result.msg
                                )
                            }
                        }
                    }).catch(error =>{
                        console.log(error);
                    })
                }else{
                    Alert.alert(
                        "Warning",
                        "Please check Your Internet connection"
                    )
                }
            })
        }
     
    }
   render(){
      
    return(
        <View style={{
            flex:1,
           
        }} >
            <View style={{
                height:170,
                width:Dimensions.get("screen").width,
                borderBottomLeftRadius:20,
                borderBottomRightRadius:20,
                backgroundColor:"#62463e",
                flexDirection:"row",
                justifyContent:'space-between',
      
            }} >
                 <Icon name="arrow-back" onPress={()=> this.props.navigation.goBack(null)} size={20} color="#FFF" style={{
                     margin:22
                 }} />
                 <Text style={{
                     fontSize:20,
                     textAlign:"center",
                     color:"#FFF",
                     margin:20
                 }}>Update Password</Text>
                 <View style={{
                     height:50,
                    width:55
                 }} >

                 </View>

            </View>

           <View style={{
             
               alignItems:'center',
               flex:1,
               height:Dimensions.get("screen").height,
            width:Dimensions.get("screen").width -45,
            position:"absolute",
            top:75,
            left:24,
            right:24,
            backgroundColor:"#FFF",
            borderTopLeftRadius:18,
            borderTopRightRadius:18
           }} >
               <TextInput 
               placeholder="Enter New Password" 
               secureTextEntry={true}
               placeholderTextColor="black"
               onChangeText={(value) => this.setState({
                   oldPasssword:value
               })}
               style={{
                   height:40,
                   width:250,
                   borderRadius:0.5,
                   borderBottomColor:"black",
                   marginTop:70
               }}
               
               />

<TextInput 
               placeholder="Re-enter New Password" 
               secureTextEntry={true}
               placeholderTextColor="black"
               onChangeText={(value) => this.setState({
                   newPassword:value
               })}
               style={{
                   height:40,
                   width:250,
                   borderBottomWidth:0.5,
                   borderBottomColor:"black",
                   marginTop:30,
                   textAlign:"left"
               }}
               
               />

               <TouchableOpacity activeOpacity={0.9} onPress={()=> this.updatePassword()} >

                   <View style={{
                       justifyContent:'center',
                       alignItems:'center',
                       backgroundColor:"#62463e",
                       height:50,
                       width:250,
                       marginTop:40,
                       borderRadius:6
                   }} >
                       <Text style={{
                           textAlign:'center',
                           color:"#FFF"
                       }} >Update Password</Text>

                   </View>
               </TouchableOpacity>

           </View>
 
        </View>
     )
   }
}