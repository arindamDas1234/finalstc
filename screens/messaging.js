import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, StatusBar,Dimensions, ScrollView, AsyncStorage, KeyboardAvoidingView  } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import Textarea from 'react-native-textarea';
import moment from "moment";
import NetInfo from "@react-native-community/netinfo";
import {URL} from '../api.js';
import { FlatList } from 'react-native-gesture-handler';
import { GiftedChat,InputToolbar  } from 'react-native-gifted-chat';
import { NetworkInfo } from "react-native-network-info";
export default class Messaging extends Component {

    constructor(props){
        super(props);

        this.state = {
            message:[],
            user_id:"",
            refresh:false,
            sendMessage:[],
            ip_address:""
        }
    }

    componentDidMount(){
       this.getMessage();

       NetworkInfo.getIPV4Address().then(ipv4Address => {
        this.setState({
            ip_address:ipv4Address
        })
      });
    }
    renderInputToolbar(props){
        // Here you will return your custom InputToolbar.js file you copied before and include with your stylings, edits.
        return(
            <InputToolbar {...props} />
        )
   }
    getMessage = () =>{
       AsyncStorage.getItem("user_id")
       .then(result =>{
          this.setState({
              user_id:result
          });

          NetInfo.fetch().then(state =>{
            if(state.isConnected){
                fetch(URL+"/get_message_list_by_order_id", {
                    headers:{
                        "Content-Type":"application/x-www-form-urlencoded"
                    },
                    method:"POST",
                    body:"order_id=" +this.props.route.params.order_id
                }).then(response => response.json())
                .then(result =>{
                    console.log(result);
                   if(result.error == false){
                    this.setState({
                        message:result.message_list,
                        refresh:false
                    })
                   }else{

                   }
                }).catch(error =>{
                    console.log(error);
                });
            }else{

            }
        })
       })
    }

    onSend = (messages) =>{
      
        NetInfo.fetch().then(state =>{
            if(state.isConnected){
                fetch("https://phpstack-525410-1692898.cloudwaysapps.com/backend/v1/insert_message_order_wise",{
                    headers:{
                        "Content-Type":"application/x-www-form-urlencoded"
                    },
                    method:"POST",
                    body:"user_id=" +this.state.user_id+ "&order_id="+ this.props.route.params.order_id+ "&message="+ messages[0].text+ "&created_by_ip="+ this.state.ip_address
                }).then(response => response.json())
                .then(result =>{
                    if(result.error === false){
                        this.getMessage()
                    }
                }).catch(error =>{
                    console.log(error);
                });
            }else{

            }
        })
    }

    hadleRefreshing = () =>{
        this.setState({
            refresh:true
        }, () => {
            this.getMessage()
        })
    }
    render(){

        return(
            <View style={{
                flex:1,
                justifyContent:"center",
                alignItems:"center"
            }} >

                <View style={{
                    flex:1,
                
                }} >
                       <StatusBar barStyle="light-content" backgroundColor="#62463e" />
                <View style={{
                    height:170,
                    width:Dimensions.get("screen").width,
                    borderBottomLeftRadius:20,
                    borderBottomRightRadius:20,
                    backgroundColor:"#62463e",
                    flexDirection:"row",
                    justifyContent:"space-between"
                }} >
                    <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.goBack(null)} >
                    <Icons name="arrow-back-outline" color="#FFF" size={18} style={{
                        margin:10
                    }} />
                    </TouchableOpacity>
                    <Text style={{
                        fontSize:18,
                        color:"#FFF",
                        margin:10,
                        textAlign:"center",
                        padding:4,
                        marginRight:10
                        
                    }} >NV2001234</Text>
                  <Icons name="refresh-circle-outline" onPress={() => this.getMessage()}  size={25} color="#FFF" style={{
                        margin:10,
                        
                    }} />
                </View>

                <View style={{
                    height:Dimensions.get("screen").height,
                    width:Dimensions.get("screen").width -45,
                    position:"absolute",
                    left:24,
                    right:24,

                    top:75,
                    flex:1,
                    backgroundColor:"#FFF",
                    borderTopLeftRadius:20,
                    borderTopRightRadius:20
                }} >
               
         <ScrollView 
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
                paddingBottom:100,
                flex:1
            }}
         >
                     <GiftedChat
       
                    alwaysShowSend
                    showUserAvatar
                    isAnimated
                    showAvatarForEveryMessage
        messages={this.state.message}
        onSend={messages => this.onSend(messages)}
     
        user={{
          _id: this.state.user_id,
        }}
      />
         </ScrollView>
                </View>

                </View>
         

                {/* <View style={{
                    height:80,
                    width:Dimensions.get("screen").width ,
                    backgroundColor:"#FFF",
                    border:0,
                    backgroundColor:"#fafafa",
                    flexDirection:"row",
                    justifyContent:'flex-start',
                    alignItems:'center',
                    borderTopWidth:0.2,
                    borderColor:"grey"
                }} >
                    <Textarea
    containerStyle={{
		height:45,
		width:"75%",
		borderWidth:0.2,
        margin:20,
        borderRadius:4,
        backgroundColor:"#fafafa",
        marginTop:20,
        padding:2
	}}
    maxLength={120}
    placeholder={' Write your message '}
    placeholderTextColor={'#c7c7c7'}
    underlineColorAndroid={'transparent'}
  />

  <View style={{
      height:40,
      width:40,
      borderRadius:20,
      backgroundColor:"#62463e",
      justifyContent:"center",
      alignItems:'center',
      padding:3
  }} >
      <Icons name="arrow-forward-outline" color="#FFF" size={20}  />

  </View>
                </View> */}
            </View>
        )
    }
}