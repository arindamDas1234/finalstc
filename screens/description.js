import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, Dimensions, StatusBar, TextInput,PermissionsAndroid,Button ,ScrollView, Alert } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Feather';

import AudioRecord from 'react-native-audio-record';
import Modal, { ModalContent,SlideAnimation } from 'react-native-modals';

import Textarea from 'react-native-textarea';

import NetInfo from "@react-native-community/netinfo";
import DropDownPicker from 'react-native-dropdown-picker';
import {URL} from '../api.js';


import Sound from 'react-native-sound';
import {Picker} from '@react-native-picker/picker';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
  } from 'react-native-indicators';
import { PickerItem } from 'react-native/Libraries/Components/Picker/Picker';



export default class Recorder extends Component {
	sound = null;

	constructor(props) {
		super(props)

		this.state = {
			playButtonStat: false,
			recordeButtonStat: false,
			recordedFile: "",
			recodePlayButtonStat: false,
			recording: false,
			loaded: false,
			pause:false,
			visible:false,
			papper_list:[]

		}
	}

	componentDidMount() {
		this.getSheet();
	}

	changePlayButtonStat1 = () => {
		this.setState({
			playButtonStat: true
		});
	}

	changePlayButtonStat2 = () => {
		this.setState({
			playButtonStat: false
		});
	}

	getSheet = ()=>{
		NetInfo.fetch().then(state =>{
			if(state.isConnected){
				fetch(URL+"/get_sheet_list",{
					headers:{
						"Content-Type":"application/x-www-form-urlencoded"
					},
					method:"GET",

				}).then(response => response.json())
				.then(result =>{
					if(result.error == false){
						this.setState({
							papper_list:result.sheet_list
						})
					}else{
						Alert.alert(
							"No data Found"
						)
					}
				}).catch(error =>{
					console.log(error);
				});
			}else{
				Alert.alert(
					"Network Error",
					"Please check Your Internet connection"
				)
			}
		})
	}

	startRecorde = async () => {
	

		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,{
					title: "STC alert",
					message: "STC need your Mic permission request",
					buttonNutral: "Ask me latter",
					buttonPositive: "ok",
					buttonNegetive: "no"
				}
			);
			console.log(granted);

			if (granted === PermissionsAndroid.RESULTS.GRANTED) {

				this.setState({
					recodePlayButtonStat: true,
					recordedFile:""
				});
				console.log(this.state.recordedFile);
				const options = {
					sampleRate: 16000,  // default 44100
					channels: 1,        // 1 or 2, default 1
					bitsPerSample: 16,  // 8 or 16, default 16
					audioSource: 6,     // android only (see below)
					wavFile: 'voice1.mp3' // default 'audio.wav'
				};
				AudioRecord.init(options);
		 	 	AudioRecord.start();
			} else {
				alert("sorry");
			}
		} catch (error) {
			console.log(error);
		}
	}

	stopRecorde = async () => {
		
		let audioFile = await AudioRecord.stop();
		console.log(audioFile);
		this.setState({
			recodePlayButtonStat: false
		});

		this.sound = new Sound(audioFile, Sound.MAIN_BUNDLE, function (error) {
			if (error) {
				console.log(error);
			}
		})
		this.setState({
			recordedFile: audioFile,
			recording:false
		});

	}

	
	playSound =  () => {
		this.setState({
			playButtonStat: true
		});
	
		if (this.sound == null) {
			alert("Please Recorde Your message first");
			this.setState({
				playButtonStat:false
			})
		} else {
			this.sound.play(success => {
				if (success) {
					console.log("audio play success");
					this.sound.getCurrentTime((seconds) => {
						if (seconds ) {
							console.log("recorde has finished");

							this.setState({
								playButtonStat: false
							});
						}
					});
				} else {
					console.log("fail to Play audio");
				}
			})
		}
		
	}

	stopSound = () => {
		this.setState({
			playButtonStat: false,
			recordeButtonStat:false
		});
		this.sound.pause();
	}

	
	render(){
		return(
			<View style={{
				flex:1
			}} >
				<View style={{
				flex:1,
				alignItems:'center'
			}} >
				<StatusBar barStyle="light-content" backgroundColor="#62463e" />
				<View style={{
					height: 170,
					width: Dimensions.get("screen").width,
					backgroundColor: "#62463e",
					borderBottomLeftRadius: 18,
					borderBottomRightRadius: 18,
					flexDirection:"row",
					justifyContent:'space-between'
				}} >
					<Icons name="arrow-back" onPress={() => this.props.navigation.goBack(null)} color="#FFF" size={18} style={{
						margin:18
					}} />
					<Text style={{
						fontSize: 18,
						margin: 20,
						color:"#FFF",
				
					}} >Post Job </Text>

					<View style={{
						height:50,
						width:50
					}} />
				</View>

				<View style={{
					height: Dimensions.get("screen").height +500,
					width: Dimensions.get("screen").width - 45,
					position: "absolute",
					top: 75,
					left: 24,
					right: 24,
					backgroundColor: "#FFF",
					borderTopLeftRadius: 20,
					borderTopRightRadius: 20,
					flex:0.5,
	

					
				}} >
			<ScrollView style={{
				marginBottom:100,
				height:Dimensions.get("screen").width
			}} >

			<Text style={{
					fontSize:16,
					margin:20,
					fontWeight:"200",
					marginLeft:22,
					marginBottom:0
				}} >Description</Text>
				 <Textarea
    containerStyle={{
		height:200,
		width:"88%",
		borderWidth:0.3,
		margin:20
	}}
    maxLength={120}
    placeholder={' Write a description for supporting image'}
    placeholderTextColor={'#c7c7c7'}
    underlineColorAndroid={'transparent'}
  />
					<View style={{
						flexDirection: "row",
						marginTop: 10,
						borderBottomWidth:0.2

					}} >
						<Text style={{
							margin:15,
							fontSize: 16,
							fontWeight: "200",
							marginLeft:24,
							marginTop:18
						}} >Recorder Voice </Text>
						{
							this.state.playButtonStat ? (
								<View style={{
									height: 40,
									width: 40,
									borderRadius: 20,
									backgroundColor: "grey",
									marginTop: 10,
									justifyContent:"center",
									
								}} >
										<Icons name="pause" style={{
									margin:8
								}} color="#FFF" size={25} onPress={() => this.stopSound()} />
							</View>
							): (
								<View style={{
									height: 40,
									width: 40,
									borderRadius: 20,
									backgroundColor: "grey",
										marginTop: 10,
									justifyContent:'center'
								}} >
										<Icons name="play" style={{
									margin:8
								}} color="#FFF" size={25}  onPress={() => this.playSound()} />
							</View>
							)
						}
						{
							this.state.recodePlayButtonStat ? (
								<View style={{
									height: 40,
									width: 40,
									borderRadius: 20,
									backgroundColor: "green",
									justifyContent: "center",
									alignItems: 'center',
									marginLeft: 12,
									marginTop:10,
									marginBottom:20
								}} >
									<Icons name="stop-circle" style={{
									margin:8
								}} color="#FFF" size={25} onPress={() => this.stopRecorde()}  />
		
								</View>
							): (
								<View style={{
									height: 40,
									width: 40,
									borderRadius: 20,
									backgroundColor: "green",
									justifyContent: "center",
									alignItems: 'center',
									marginLeft: 12,
									marginTop:10,
									marginBottom:20
								}} >
									<Icons name="mic" style={{
									margin:8
								}} color="#FFF" size={25} onPress={() => this.startRecorde()}  />
		
								</View>
							)
						}
						<View style={{
							height: 40,
							width: 40,
							borderRadius: 20,
							backgroundColor: "grey",
							justifyContent: "center",
							alignItems: "center",
							marginTop: 10,
							marginLeft:12
						}} >
							<Icon name="x" onPress={() => {
								if(this.sound == null){
									alert("Please Recorde First")
								}else{

								Alert.alert(
									"Confirmation Alert",
									"Are you sure to delete audio record? ?",
									[
										{
											text:"Ok",
											onPress: async () => {
												await this.sound.release()
												alert("Recorde Successfully")
											},
											style:"default"
										},
										{
											text:"Cancel",
											onPress: () => null,
											style:"cancel"
										}
									]
								)
									// this.sound.release()
									
								}
							}} size={25} color="#FFF" style={{
								margin:8
							}} />

						</View>

					</View>
					<View style={{
						flexDirection: "row",
						justifyContent: 'space-between',
						marginTop: 10
				}} >
					<Text style={{
						fontSize: 16,
							fontWeight: "100",
							marginTop:15,
						margin:25
						}} >Media</Text>
{/* 
<Picker
  style={{height: 50, width: 200,  borderWidth:0.3, borderColor:"black", borderRadius:6}}
  onValueChange={(itemValue, itemIndex) =>
    this.setState({language: itemValue})
  }>
 
							<Picker.Item label='A-VINYL PAPER BACK-PLAIN -45.5"' value="js" />
							<Picker.Item label='C-VINIL PAPER BACK-CANVAS TEXTURE -40.5"' value="js" />
							
</Picker> */}
<View style={{
	height:50,
	width:200,
	justifyContent:"center",
	alignItems:"center",
	borderWidth:0.5,
	marginRight:20
}} >
	<Picker 
	style={{height:50,width:200}}
	onValueChange={(value) => console.log(value)}
	>
		{
			this.state.papper_list.map(value =>(
				<Picker.Item label={value.sheet_name.substring(0, 18)} value={value.id}  />
			))
		}

	</Picker>

</View>
				</View>

				<View style={{
					flexDirection:"row",
					justifyContent:"space-between"
				}} > 

				<View style={{
					justifyContent:"center"
				}} />

<View style={{
	justifyContent:"center",
	center:"center"
}} >
	{/* <TouchableOpacity onPress={() => this.setState({
					visible:true
				})} >
				<View style={{
					
					height:40,
				    width:260,
				    marginHorizontal:25,
					marginTop:45,
					borderRadius:10,
					backgroundColor:"#62463e",
					alignItems:'center',
					marginBottom:200
					
				}} >

				<Text style={{
					lineHeight:45,
					 fontSize:18,
					 color:"#FFF"
				}} >Submit</Text>

				</View>
				</TouchableOpacity> */}

</View>

				<View style={{
					height:40,
					width:10
				}} />

				</View>
				 <Modal
    visible={this.state.visible}
    onTouchOutside={() => {
      this.setState({ visible: false });
    }}
  >
    <ModalContent style={{
		height:280,
		width:340
	}} >
      <View style={{
		
		  alignItems:"center",
	  }} >
		 <Image source={require("../assets/tick.png")} style={{
			  height:80,
			  width:80,
			  
		  }} />
		  <Text style={{
			  textAlign:'center',
			  fontSize:20
		  }} >Thank You</Text>

		  <Text style={{
			  textAlign:'center',
			  alignItems:'center',
			  fontSize:16
		  }} >Thank your for Submiting job</Text>
		  <Text style={{
			  textAlign:'center',
			  alignItems:'center',
			  fontSize:16
		  }} >Your Preview will be ready soon</Text>

		 <TouchableOpacity onPress={() => this.setState({
			 visible:false
		 })} >
		 <View style={{
			  height:40,
			  width:150,
			  backgroundColor:"#62463e",
			justifyContent:"center",
			alignItems:"center",
			marginTop:20
		  }} >
			  <Text style={{
				  textAlign:"center",
				  fontSize:18,
				  color:"#FFF"
			  }} >Great</Text>

		  </View>
		 </TouchableOpacity>
		  </View>
    </ModalContent>
  </Modal>
  
			</ScrollView>

				</View>



			</View>

				<TouchableOpacity activeOpacity={0.9} >
					<View style={{
						height:50,
						width:Dimensions.get("screen").width,
						backgroundColor:"#62463e",
						justifyContent:"center",
						alignItems:'center'
					}} >
						<Text style={{
							textAlign:"center",
							color:"#FFF",
							fontSize:18
						}} >Submit</Text>

					</View>
				</TouchableOpacity>

			</View>
		)
	}
}