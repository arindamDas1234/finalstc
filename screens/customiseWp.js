import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, Dimensions, ScrollView,KeyboardAvoidingView,PermissionsAndroid,FlatList, Button,Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import TabComponnet from '../screens/TabnarComponent.js';

import TaqbContainer from '../screens/TabnarComponent.js';
import ImagePicker from 'react-native-image-crop-picker';
import uuid from 'uuid-random';

let imagesGrid = []
export default class customiseComponent extends Component{
    constructor(props){
        super(props)

        this.state = {
            imageUpload:"",
            photo:"",
            photo2:"",
            Quantity:"1",
            height:"",
            width:"",
            imageObj2:{},
            photo2Arr:[],
            imageId:1
        }
    }

    componentDidMount(){
        if(this.props.route.params.image){
            this.setState({
                photo:this.props.route.params.image,
                imageObj2:this.props.route.params.imageObj
            });
        }
    }

    handleChooseImage = async () =>{
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title:"Stc alert",
          message:"Stc need to Access your File",
          buttonNutral:"ask Me latter",
          buttonNegetive:"No",
          buttonPositive:"yes"
        }
      );

      if(granted === PermissionsAndroid.RESULTS.GRANTED){
        ImagePicker.openPicker({
  multiple: true
}).then(image => {
  console.log(image);
  let tempArray = [];
  image.forEach(value =>{
    var image = {
      uri:value.path,
      id:this.state.imageId++
   };
    tempArray.push(image);
    this.setState({
    photo2Arr:[...this.state.photo2Arr,...tempArray]
    });
    console.log(photo2Arr);
  })

  
 
  console.log(tempArray);
});
      }else{
        alert("Please try again latter");
      }
      }

      launchCamera = async () =>{
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,{
            title:"STC alert",
            message:"STC needs to access your Camera ",
            buttonNutral:"ask me latter",
            buttonPositive:"Yes",
            buttonNegetive:"No"

          }
        );

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("yes");
          ImagePicker.openCamera({
  width: 300,
  height: 400,
  cropping: false,
          }).then(image => {
  
  this.setState({
    photo:image.path
  });
});
        }else{
          alert("Please Try again Latter");
        }
    }

launchCamera2 = async () =>{
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,{
            title:"STC alert",
            message:"STC needs to access your Camera ",
            buttonNutral:"ask me latter",
            buttonPositive:"Yes",
            buttonNegetive:"No"

          }
        );

        if(granted === PermissionsAndroid.RESULTS.GRANTED){
          ImagePicker.openCamera({
  width: 300,
  height: 400,
  cropping: false,

}).then(image => {

 var ImageObj = {
    uri: image.path,
    id: uuid()
 };


  let tempImage = [];
  tempImage.push(ImageObj);
  console.log(tempImage);

  this.setState({
    photo2Arr:[... this.state.photo2Arr, ... tempImage]
  });

});
        }else{
          alert("Please Try again Latter");
        }
    }
    quantity1 = (qty) =>{
        let q = Number(qty)
        this.setState({
            Quantity:q +1
        });
    }

    quantity2 = (qty) =>{
        let q = Number(qty)
        if(qty <= 1){

        }else{
            this.setState({
                Quantity:q-1
            });
        }
    }

    orderSubmit =  ()=>{

      this.props.navigation.navigate("description");
      // return;
      // alert(this.state.height)
      // let filename = "";
      // let url ="";
      // let extensions ="";
      // if(this.props.route.params.imageObj){
      // filename = this.props.route.params.imageObj.image.replace(/^.*[\\\/]/, '');
      // url = this.props.route.params.imageObj.image;
      // extentions = filename.split(".").pop();   

      //  let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJuYW1lIjoiQXJpbmRhbSBEYXMifSwiaWF0IjoxNjA3OTM4NDE0fQ.6ZHTw_RmR0PcWasMc8euVjIh_PxfFaoldTCyRlUFUlY";

      // const imge = {
      //               uri:url,
      //               type:'image'+'/'+extentions,
      //               name:filename
      //               }
                  


      // var  payload = new FormData();
      // payload.append("image",imge);
      // payload.append("height",this.state.height);
      // payload.append("width",this.state.width);
      

      // fetch("http://192.168.0.9:5000/api/orders/create_custom_order",{
      //                   method:"POST",
      //                   headers:{          
      //                      Accept: "application/json",
      //                      'Content-Type': 'multipart/form-data',
      //                   },
            
      //                body:payload
                        
      //               }).then(response =>{
      //                   console.log(response);
      //               }).catch(error =>{
      //                   console.log(error);
      //               })   
      // }else{
       
      // }
     
    }
      

    selectImageFromGalary = async()=>{
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title:"Stc alert",
          message:"Stc need to Access your File",
          buttonNutral:"ask Me latter",
          buttonNegetive:"No",
          buttonPositive:"yes"
        }
      );

      if(granted === PermissionsAndroid.RESULTS.GRANTED){
        ImagePicker.openPicker({
  multiple: true
}).then(image => {
  console.log(image);
  let tempArray = [];
  image.forEach(value =>{
    var image = {
      uri:value.path,
      id:uuid()
   };
    tempArray.push(image);
  })
  this.setState({
    photo2Arr:[...this.state.photo2Arr,...tempArray]
    });
   

  console.log(tempArray);
});
      }else{
        alert("Please try again latter");
      }
    }

    selectMultiPleImage = () =>{
      console.log("success")
    }

    addMore = async () =>{
        const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title:"Stc alert",
          message:"Stc need to Access your File",
          buttonNutral:"ask Me latter",
          buttonNegetive:"No",
          buttonPositive:"yes"
        }
      );

      if(granted === PermissionsAndroid.RESULTS.GRANTED){
        ImagePicker.openPicker({
  multiple: false
}).then(image => {
  console.log(image);
  let tempArray = [];
  image.forEach(value =>{
    var image = {
      uri:value.path,
      id:uuid()
   };
    tempArray.push(image);
    this.setState({
    photo2Arr:[...this.state.photo2Arr,...tempArray]
    });
    console.log(photo2Arr);
  })

  
 
  console.log(tempArray);
});
      }else{
        alert("Please try again latter");
      }
    }

  deleteImage = async (id) => {
  
      Alert.alert(
        "STC alert",
        "Are you sure to Delete custom image ??",
        [
          {text:"yes", onPress: async() =>{
             let newArray = [];
      newArray = this.state.photo2Arr;

            const deltedItem = newArray.find(item => item.id === id);
        const index =  await newArray.indexOf(deltedItem);
        newArray.splice(index,1);
       if(index > -1){

          this.setState({
            photo2Arr:newArray
          });
       }
     }},
     {text:"No", onPress:() => Alert.alert("Continue Process"), style:"cancel"}
        ],
        {
          cancelable:true
        }
        )
     

      

        console.log(this.state.photo2Arr);
  }
  uploadFile = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title:"Stc alert",
        message:"Stc need to Access your File",
        buttonNutral:"ask Me latter",
        buttonNegetive:"No",
        buttonPositive:"yes"
      }
    );

    if(granted === PermissionsAndroid.RESULTS.GRANTED){
      ImagePicker.openPicker({

}).then(image => {
  this.setState({
    photo: image.path
  });
});
    }else{
      alert("Please try again latter");
    }
  }
    render(){
      
        return(
            <View style={{
                flex:1,
                alignItems:'center'
            }} >
                <View style={{
                flex:1,

              
            }} >
                
                <View style={{
    height:170,
    width:Dimensions.get("screen").width,
    backgroundColor:'#62463e',
    borderBottomLeftRadius:20,
    borderBottomRightRadius:20,
                flexDirection: "row",
    justifyContent:"space-between"
    
}} >
  <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.navigate("home")} >
  <Icon name="arrow-back" color="#FFF" size={20} style={{
        margin:20
    }} />
  </TouchableOpacity>
    <Text style={{
     
        fontSize:18,
        color:"#FFF",
        margin:18,
                }} >Customized  Wallpaper</Text>
                <View style={{
                  height: 40,
                  width:50
                }} ></View>

</View>

<View style={{
   height:Dimensions.get("screen").height,
   width:Dimensions.get("screen").width -38,
   position:"absolute",
   top:78,
   left:20,
   right:0,
   alignItems:"center",
   justifyContent:"center",
   flex:0.6,

   backgroundColor:"#FFF",
   borderTopRightRadius:18,
   borderTopLeftRadius:18,
   flex:1

}} >
    <ScrollView 
        contentContainerStyle={{
          paddingBottom:120
        }}
        showsVerticalScrollIndicator={false}
    >
    <View style={{
                    justifyContent: 'center',
      flex:1

    }} >
       <Text style={{
        fontSize:16,
        margin:13,
        color:"#404040"
        
    }} >Select Image </Text>

        <View style={{
            flexDirection:"row",
            alignItems:'center'

        }} >
            {
                this.state.photo ? (
                    <View style={{
                      height:110,
                      width:120,

                        backgroundColor:"#fafafa",
                            elevation: 5,
                        marginHorizontal:10
                    }} >
                        <Image source={{uri:this.state.photo}} style={{
                                height:110,
                                width:120,
                                marginBottom:40
                            }} />
        
                    </View>
                ) :(
                    <View style={{
                      height:110,
                      width:120,
                        elevation:5,
                        marginHorizontal:10
                    }} >
                       <Image source={require("../assets/uload3.jpg")} style={{
                        height:110,
                        width:120,
                        marginLeft:6
                       }} /> 
        
                    </View>
                )
            }
            
           <View style={{
               flexDirection:"column"
           }} >


                <View style={{
                flexDirection:'row'
            }} >
                <View style={{
                    backgroundColor:'#62463e',
                    height:40,
                    width:40,
                    borderRadius:20,
                    marginTop:5,
                    justifyContent:'center',
                    alignItems:'center',
                    marginLeft:10
                }} >
                   <TouchableOpacity
                   activeOpacity={2} 
                    onPress={() => this.props.navigation.navigate("customCatelog")}
                   >
                   <Image source={require("../assets/menu.jpg")} style={{
                    height:20,
                    width:28,
                }}/>
                   </TouchableOpacity>

                </View>
              <TouchableOpacity  activeOpacity={2} onPress={() => this.props.navigation.navigate("customCatelog")} >
                <Text style={{
                    textAlign:"center",
                  
                    margin:12,
                    fontSize:16,
                  color:"#404040"

                }} >
                    Select A Pattern
                </Text>
              </TouchableOpacity>

            </View>
            
                <Text style={{
                    
                    fontSize:16,
                    color:'grey',
                    marginLeft:80,
                    fontSize:14
                }} >------ Or ------</Text>

<View style={{
                flexDirection:'row'
            }} >
                <View style={{
                    backgroundColor:'#62463e',
                    height:40,
                    width:40,
                    borderRadius:20,
                    marginTop:5,
                    justifyContent:'center',
                    alignItems:'center',
                    marginLeft:10
                }} >
                  <TouchableOpacity activeOpacity={2} onPress={() => this.uploadFile()} >
                  <Icon name="camera-outline" size={18} color="#FFF" />
                  </TouchableOpacity>

                </View>
               <TouchableOpacity onPress={() => this.uploadFile()} >
                 <Text style={{
                    textAlign:"center",
                    margin:12,
                    marginHorizontal:20,
                    fontSize:16,
                    color:"#404040"

                }} >
                    Upload File
                </Text>

               </TouchableOpacity>
            </View>
           </View>
                
        </View>
    

     <View style={{
      justifyContent:'center',
      alignItems:"center",

     }} >
        <Image source={require("../assets/desc_01.jpg")} style={{
            height:145,
            width:235,
            
            marginTop:10,
            marginBottom:20,
            marginRight:6
     

           }} />
     </View>

     
       <View style={{
           flexDirection:'row'
       }} >
          <View style={{
              flexDirection:'column'
          }} >
               <Text style={{
               fontFamily:'Roboto-Bold',
               margin:4,
               fontSize:16,
               color:"#404040"
           }} >Wall Dimension :</Text>
           <Text style={{
               textAlign:'center',
               fontColor:'grey',
               fontSize:12,
           }} >(in inches)</Text>

          </View>
    <View style={{
        flexDirection:'row'
    }} >
        <Text style={{
            fontSize:13,
            margin:9
            
        }} >W</Text>
         <TextInput
            placeholder="Width"
            defaultValue={this.state.height.toString()}
            onChangeText={(value) => this.setState({
              height:value
            })}
            style={{
                width:60,
                height:40,
                borderWidth:0.2,
                borderRadius:5,
                marginHorizontal:2,
                textAlign:'center'
            }}
            keyboardType="numeric"
          />
          <Text style={{
              fontWeight:'bold',
              fontSize:16,
              marginTop:10,
              marginLeft:10
          }} > × </Text>

<Text style={{
            fontSize:13,
            margin:10
            
        }} >H</Text>
         <TextInput
            placeholder="Height"
             onChangeText={(value) => this.setState({
                  width:value
                })}
            style={{
                width:60,
                height:40,
                borderWidth:0.2,
                borderRadius:5,
                
                textAlign:'center'
               
            }}
            defaultValue={this.state.width.toString()}
            keyboardType="numeric"
          />
    </View>

       </View>

       <View style={{
           flexDirection:"row",
           alignItems:"center"
       }} >
            <Text style={{
            fontFamily:'Roboto-Bold',
            fontSize:16,
            margin:8,
            marginTop:25, 
            color:"#404040"
        }} >Quantity :</Text>

                <View style={{
                    flexDirection:"row",
                    marginLeft:30,
                    marginTop:20
                   
                   
                }} >
                    <TouchableOpacity activeOpacity={2} onPress={() => this.quantity2(this.state.Quantity)} >
                    <View style={{
                        justifyContent:'center',
                        alignItems:"center",
                        height:28,
                        width:28,
                        borderRadius:28 /2,
                        borderWidth:0.4,
                        borderRadiusColor:'black',
                        marginHorizontal:10
                    }} >
                        <Text style={{
                            textAlign:'center',
                            fontSize:15
                        }} >-</Text>

                    </View>
                    </TouchableOpacity>
                  <View style={{
                      width:100,
                      height:30,
                      borderRadius:13,
                      borderColor:'black',
                      borderWidth:0.3
                  }} >
                      <Text style={{
                          textAlign:"center",
                          marginTop:5
                      } }>{this.state.Quantity}</Text>
                  </View>



                    <TouchableOpacity activeOpacity={2} onPress={() => this.quantity1(this.state.Quantity )} >
                    <View style={{
                         justifyContent:'center',
                         alignItems:"center",
                         height:28,
                         width:28,
                         borderRadius:28 /2,
                         borderWidth:0.4,
                         borderRadiusColor:'black',
                         marginHorizontal:10
                    }} >
                        <Text style={{
                            textAlign:'center',
                            fontWeight:'bold',
                            fontSize:13
                        }} >+</Text>

                    </View>
                    </TouchableOpacity>

                </View>
       </View>
      
   <View style={{
    justifyContent:'center',
    alignItems:"center"
   }} >
    <Text numberOfLines={2} style={{
       fontSize:16,
       marginTop:14,
      width:300,
    lineHeight:20,
    color:"#404040",
 textAlign:"center"

    }} >Maximum 300dpi with size of 6mb to 7mb required in pdf or ai format</Text>

   </View>
<View style={{
    borderBottomWidth:0.6,
    borderBottomColor:'black',
    marginTop:40,
                        alignItems: 'center',
    justifyContent:"center"
}} />

<Text style={{
 
     
       fontSize:16,
       marginTop:14,
       textAlign:"center",
       marginBottom:8,
  color:"#404040"
    }} >Add Supporting Images (Custom or wall Image) </Text>



{
  this.state.photo2  ? (

  <Image source={{uri:this.state.photo2}} style={{
    height:120,
    width:120
  }} />
  ) :(null)
}
                    {
                      this.state.photo2Arr.length > 0 ? (
                        <FlatList

showsHorizontalScrollView={false}
data={this.state.photo2Arr}
horizontal
contentContainerStyle={{
  paddingBottom:20
}}
renderItem={(items, index) =>{
 console.log(items.item.id)
  return(
  <View style={{
    justifyContent:"center",
    alignItems:"center",
    flex:1
  }} >
  <TouchableOpacity onPress={() => this.deleteImage(items.item.id)} >

  <Image source={{uri:items.item.uri}} style={{
    height:140,
    width:160,
    margin:12
        }} />
        <Text style={{
          textAlign:"center"
        }} >Attachment</Text>

  <Icon name="close" style={{
    position:"absolute",
    top:70,
    left:80,
    right:0
  }} size={30} color="red"  />
  </TouchableOpacity>

  </View>
  )
}}
keyExtractor={(item) => item.id}

 />

                      ):(null)
}


<View style={{
  flexDirection: "row",
        marginBottom: 30,
        justifyContent: "center",
  alignItems:"center"

}} >


<TouchableOpacity activeOpacity={2} onPress={() => this.launchCamera2()} >
<Text style={{

 fontSize:15,

marginTop: 15,
 marginHorizontal:10
}} >Capture Image</Text>
</TouchableOpacity>

<View style={{
backgroundColor:'#62463e',
height:40,
width:40,
borderRadius:20,
marginTop:10,
justifyContent:'center',
                          alignItems: 'center',
marginHorizontal:10
}} >
<TouchableOpacity activeOpacity={2} onPress={() => this.launchCamera2()} >
<Icon name="camera-outline" size={18} color="#FFF" /> 
</TouchableOpacity>


</View>



<TouchableOpacity activeOpacity={2} onPress={() => this.selectImageFromGalary()} >
  <View style={{
backgroundColor:'#62463e',
height:40,
width:40,
borderRadius:20,
marginTop:10,
justifyContent:'center',
                            alignItems: 'center',
marginHorizontal:5

}} >
<Image source={require("../assets/menu.jpg")} style={{
height:20,
width:20,

}}/>

</View>
    </TouchableOpacity>
    <TouchableOpacity activeOpacity={2} onPress={() => this.selectImageFromGalary()} >
<Text style={{
 
 fontSize:15,

  marginTop: 15,
marginHorizontal:10

}} >Select Image</Text>
</TouchableOpacity>

                      </View>
                      <Text style={{
            textAlign:"center",
            marginBottom:90
          }} >Any special image to be appended on Wall</Text>
                    </View>
                   
                </ScrollView>

</View>

            </View>
   <TouchableOpacity activeOpacity={2} onPress={() => this.orderSubmit()} >
          <View style={{

  alignItems:'center',
  height:45,
width:Dimensions.get("screen").width,

backgroundColor:"#62463e",
justifyContent:"center"
 }} >
<Text
  style={{
    textAlign:"center",
    alignItems:'center',
    color:"#FFF",
    fontSize:18,

  }}
>Submit</Text>
 </View>
   </TouchableOpacity>
            </View>
        );
    }
}