import React, { Component  } from 'react';

import { StyleSheet, View, Image, TouchableOpacity, Text, Dimensions, StatusBar, FlatList, Alert, AsyncStorage	 } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Transition, Transitioning } from 'react-native-reanimated';
import IconName from 'react-native-vector-icons/FontAwesome';
import NetInfo from "@react-native-community/netinfo";
import {URL, imageUrl} from '../api.js';
import moment from "moment";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Modal, { ModalContent,SlideAnimation } from 'react-native-modals';

const transition = () =>{
	<Transition.Together>
		<Transition.In type="fade" durationMs={300} />
		<Transition.Change />
		<Transition.Out type="fade" durationMs={300} />
	</Transition.Together>
}


export default class onGoingJobList extends Component{

	constructor(props){
		super(props);
	
		this.state = {
			jobList:[],
			jobPreview_by_id:"",
			current_index:null,
			isModelShow:false,
			jonPreviewImage:"",
			jobPreview_order_id:"",
			is_model_view:false,
			order_id:"",
			user_id:"",
			confirm_data:[
				{
					label:"Non payment",
					value:"Non payment"
					
				},
				{
					label:"Details missing",
					value:"Details payment"
				}
			],
			user_role:"",
			desc:""
		}
	}

    componentDidMount(){
		this.getJobList();
	}
	
	set_job_view_by_id = () =>{

	}
getJobList = () =>{
	NetInfo.fetch().then(state =>{
		if(state.isConnected){
			AsyncStorage.getItem("user_id")
			.then(result =>{
				this.setState({
					user_id:result
				})
				console.log("user_id"+ result);
				fetch(URL+"/get_all_job_by_user_id",{
					headers:{
						"Content-Type":"application/x-www-form-urlencoded"
					},
					method:"POST",
					body:"user_id=" +result
				}).then(response => response.json())
				.then(result =>{
					console.log(result);
					if(result.error ==false){
						this.setState({
							jobList:result.order_details
						});
					}
				}).catch(error =>{
					console.log(error);
				})
			}).catch(error =>{	
				console.log(error);
			})
		}else{

		}
	})
}

cancelJob = (id) => {
	// check user role
	this.setState({
		order_id:id
	})
	AsyncStorage.getItem("user_id")
	.then(result =>{
		if(result){
			this.setState({
				user_id:result
			})
			NetInfo.fetch().then(state =>{
				if(state.isConnected){
					fetch(URL+"/get_user_role_by_user_id",{
						headers:{
							"Content-Type":"application/x-www-form-urlencoded"
						},
						method:"POST",
						body:"user_id=" +result
					}).then(response => response.json())
					.then(result =>{
						if(result.user_role_name === "Distributer"){
							this.setState({
								user_role:result.user_role_name,
								is_model_view:true
							})
						}else{
							Alert.alert(
								"Confirmation Eror",
								"Are You Sure to Delete Message"
							)
						}
					}).catch(error =>{
						console.log(error);
					})
				}else{
					Alert.alert(
						"Network Error",
						"Please check Your Internet connection",

						[
							{
								text:"Cancel",
								onPress:() => null,
								style:"cancel"
							},
							{
								text:"Yes",
								onPress:() => this.doneCancle(),
								style:"default"
							}
						]
					)
				}
			})
		}else{

		}
	}).catch(error =>{
		console.log(error);
	})
}

doneCancle = () => {
	if(this.state.desc === ""){
		Alert.alert(
			"Validation Error",
			"Please Select one condition"
		);
		return;
	}
	NetInfo.fetch().then(state =>{
		if(state.isConnected){
			fetch(URL+"/get_cancelled_job_by_user_id",{
				headers:{
					"Content-Type":"application/x-www-form-urlencoded"
				},
				method:"POST",
				body:"order_id=" +this.state.order_id+ "&user_id="+ this.state.user_id+ "&created_by_ip="+"1111"
			}).then(response => response.json())
			.then(result =>{
				if(result.error == false){
					this.setState({
						is_model_view:false
					});

					Alert.alert(
						"Success",
						"Order Cancled succesfully"
					)
				}else{
					Alert.alert(
						"Error",
						"Order Cancel not success"
					)
				}
			}).catch(error =>{
				console.log(error);
			})
		}else{
			Alert.alert(
				"Network Error",
				"Please check Your Internet connection"
			)
		}
	})
}

	check_job_view = (id) =>{
		if(id === this.state.jobPreview_by_id){
			return true
		}else{
			return false
		}
	}

	JobPreview = (image, order_id) =>{
		console.log(order_id);
		this.setState({
			isModelShow:true,
			jonPreviewImage:image,
			jobPreview_order_id:order_id
		})
	}
    render(){
        return(
			<View style={{
				flex:1
			}} >
				<View style={{
					flex:1
				}} >

				<Transitioning.View
			transition={transition}
			
			style={{
              
                flex:1
            }} >
              <View style={{
				  height:170,
				  width:Dimensions.get("screen").width,
				  backgroundColor:"#62463e",
				  borderBottomLeftRadius:20,
				  borderBottomRightRadius:20,
				  flexDirection:"row",
				  justifyContent:'space-between'
			  }} >
				 <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.goBack(null)} >
				 <Icon name="arrow-back" size={18} color="#FFF" style={{
					  margin:20
				  }} />
				 </TouchableOpacity>
				  <Text style={{
					  textAlign:"center",
					  color:"#FFF",
					  fontSize:18,
					  margin:20
				  }} >In Progress Job</Text>
				  <View style={{
					  height:50,
					  width:50
				  }} >

				  </View>

			  </View>

			  <View style={{
				  height:Dimensions.get("screen").height,
				  width:Dimensions.get("screen").width -45,
				  backgroundColor:"#FFF",
				  borderTopLeftRadius:20,
				  borderTopRightRadius:20,
				  position:"absolute",
				  top:75,
				  left:24,
				  right:24,
				 
			  }} >
				{
					this.state.jobList.length >0 ? (
						<View>
							<FlatList
						data={this.state.jobList}
						contentContainerStyle={{
							paddingBottom:200
						}}
					   showsVerticalScrollIndicator={false}
						renderItem={(items, index) =>{
							console.log(items.item.support_image.image_details);
							return(
								<View style={{
									justifyContent:"center",
									alignItems:'center',
									marginTop:15,
									flexGrow:1
								}} >
									<View style={{
								   flexGrow:1,
								   width:Dimensions.get("screen").width * 0.75,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
									borderRadius:8,
									backgroundColor:"#FFF",
									elevation:5,
									marginBottom:20
								}} >
								   <View style={{
									   padding:10,
									   borderBottomWidth:0.5,
   
								   }} >
										<Text style={{
									   fontSize:14,
									   fontWeight:"normal", 
									   padding:4
									}} >{items.item.order_id}</Text>
									<Text style={{
										color:"#ff9800",
										fontSize:14,
										fontWeight:"bold",
										padding:2
									}} >{  moment(Date(items.item.date_time)).format("MMMM Do YYYY, h:mm:ss a") }</Text>
								   
								   <View style={{
									   flexDirection:"row",
   
								   }} >
									   <Text style={{
									   fontSize:12,
									   color:"grey",
									   padding:4
								   }} >Pattern ID :</Text>
								   <Text style={{
									   fontSize:12,
									   
									   padding:4
								   }} >{ items.item.pattern_no }</Text>
   
									   </View>
   
									   <View style={{
									   flexDirection:"row",
   
								   }} >
									   <Text style={{
									   fontSize:12,
									   color:"grey",
									   padding:4
								   }} >Distributer:</Text>
								   <Text style={{
									   fontSize:12,
									   
									   padding:4
								   }} >{ items.item.distributer }</Text>
   
									   </View>
   
									   <View style={{
									   flexDirection:"row",
   
								   }} >
									   <Text style={{
									   fontSize:12,
									   color:"grey",
									   padding:4
								   }} >Dealer       :</Text>
								   <Text style={{
									   fontSize:12,
									   
									   padding:4
								   }} >{ items.item.first_name }</Text>
   
									   </View>
   
									   <View style={{
									   flexDirection:"row",
   
								   }} >
									   <Text style={{
									   fontSize:12,
									   color:"grey",
									   padding:4
								   }} >quantity    :</Text>
								   <Text style={{
									   fontSize:12,
									   
									   padding:4
								   }} >{ items.item.quantity }</Text>
   
									   </View>
   
   
									   </View>
									   <TouchableOpacity activeOpacity={0.5} onPress = {() =>{
											   
											   this.setState({
												   current_index: this.state.current_index === items.item.id ? null : items.item.id
											   })
									   }} >
									   <Text style={{
								   textAlign:"center",
								   color:"blue",
								   marginTop:10,
								   fontWeight:"bold",
								   marginBottom:20
								   
							   }} >View Details</Text>
									   </TouchableOpacity>
   
									   {
										   this.state.current_index === items.item.id ? (
											   <View>
												   <View style={{
												   borderBottomWidth:0.5,
												   marginTop:12
											   }} />
		   
											   
												   
		   
												   <View style={{
													   flexDirection:"row",
													   alignItems:'center',
													   justifyContent:"space-around",
													   marginTop:20
												   }} >
													  <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.navigate("postViewJob",{
														  pattern_number:items.item.pattern_no,
														  order_image:items.item.sub_category_img_url,
														  supportive_image:items.item.support_image.image_details,
														  button_show:items.item.button_show,
														  order_id:items.item.id
													  })}  >
													  <View style={{
													   justifyContent:'center',
												   
													   alignItems:'center'
												   }} >
													   <Icon name="attach-outline" style={{
														   margin:10,
													   
													   }} size={20} />
													   <Text style={{
														   fontSize:15,
														   marginLeft:20
													   }}>Preview Job</Text>
   
													   </View>
													  </TouchableOpacity>
												   
													  <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.navigate("messaging",{
														  order_id:items.item.id
													  }) } >
															 <View style={{
													   justifyContent:'center',
													   alignItems:'center'
												   }} >
													   <Icon name="calendar-outline" style={{
														   margin:10
													   }} size={20} />
													   <Text style={{
														   fontSize:15
													   }}> View Message</Text>
   
													   </View>
													  </TouchableOpacity>
   
													   </View>
   
   
													   <View style={{
													   flexDirection:"row",
													   alignItems:'center',
													   justifyContent:"space-around",
													   marginTop:25,
													   marginBottom:30
												   }} >
													  <TouchableOpacity activeOpacity={2} onPress={() => this.props.navigation.navigate("tracker",{
														  order_id:items.item.id
													  })}  >
													  <View style={{
													   justifyContent:'center',
													 alignItems:'center',
										   
												   }} >
													   <Icon name="refresh-outline" style={{
														   margin:10
													   }} size={20} />
													   <Text style={{
														   fontSize:15,
												   
													   }}> View Histroy</Text>
   
													   </View>
													  </TouchableOpacity>
   
													  <TouchableOpacity onPress={() => this.cancelJob(items.item.id)} >
													  <View style={{
													   justifyContent:'center',
													   alignItems:'center'
												   }} >
													   <IconName name="ban" style={{
														   margin:10
													   }} size={20} />
													   <Text style={{
														   fontSize:15
													   }}> Cancel Job</Text>
   
													   </View>
   
													  </TouchableOpacity>
													   </View>
												   </View>
										   ) :null
									   }
									</View>
									
									</View  > 
							)
						}}
						keyExtractor={(item) => item.id}
					/>
					<View style={{
						height:50
					}} />
							</View>
					) :(
						<View style={{
							justifyContent:'center',
							alignitems:"center",
							flex:0.6
						}} >
						<Text style={{
							textAlign:'center',
							fontSize:18,
							
						}} >No Post Found</Text>

						</View>
					)
				}

			  </View>
     
            </Transitioning.View>

				</View>

				<View style={{
			 flexDirection:"row",
			 justifyContent:"space-between",
			 alignitems:'center',
			 backgroundColor:"#FFF",
			 height:50,
			 width:Dimensions.get("screen").width,
			 elevation:3,
			 borderTopWidth:0.3,
			 borderTopColor:"#eeee"
		 }} >
			<TouchableOpacity activeOpacity={2} onPress={() => alert("success")} >  
			<Text style={{
				 textAlign:"center",
				 padding:20
			 }} >All</Text>
			</TouchableOpacity>

<TouchableOpacity activeOpacity={2} onPress={() => alert("success")} >
<Text style={{
				 textAlign:"center",
				 padding:20
			 }} >In Progress</Text>
</TouchableOpacity>

<TouchableOpacity activeOpacity={2} onPress={() => alert("success")} >
<Text style={{
				 textAlign:"center",
				 padding:20
			 }} >Complete</Text>
</TouchableOpacity>

<TouchableOpacity activeOpacity={2} >
<Text style={{
				 textAlign:"center",
				 padding:20
			 }} >Cancel</Text>
</TouchableOpacity>


		 </View>

		 <Modal
		 style={{
			 height:330,
			 
		 }}
		 onTouchOutside={() => {
			this.setState({ is_model_view: false });
		  }}
		 ButtomModal
		 
    visible={this.state.is_model_view}
    modalAnimation={new SlideAnimation({
      slideFrom: 'bottom',
    })}
  >
    <ModalContent>
      <View style={{
		  
		  alignitems:'center',
		  justifyContent:"center"
	
	  }} >
		  <Text style={{
			  textAlign:"center",
			  fontSize:18,
			  fontWeight:"bold",
			  marginTop:18
		  }} >Why do you want to cancel this job?
		  </Text>
	<View style={{
		marginHorizontal:30,
		marginTop:30
	}} >
		<RadioForm
  radio_props={this.state.confirm_data}
  initial={0}
  formHorizontal={true}
  labelHorizontal={false}
  buttonColor={'#2196f3'}
  animation={true}
  borderWidth={0.6}
          buttonInnerColor={'#e74c3c'}
		  containerStyle={{
			  marginTop:20,
			  marginLeft:30
		  }}
         
          buttonSize={20}
          buttonOuterSize={25}
          buttonStyle={{
			  margin:20,
			  marginTop:20
		  }}
          buttonWrapStyle={{marginLeft: 0}}
  onPress={(value) => this.setState({
	  desc:value
  })}
/>
	</View>

<TouchableOpacity onPress={() => this.doneCancle()} activeOpacity={2}  >

<View style={{
		height:40,
		width:160,
		backgroundColor:"green",
		justifyContent:"center",
		alignItems:"center",
		marginHorizontal:40,
		marginTop:20
	}} >
		<Text style={{
			textAlign:"center",
			color:"#FFF",
			fontSize:16
		}} >Done</Text>
	</View>

</TouchableOpacity>
<TouchableOpacity onPress={() => this.setState({
	is_model_view:false
})} style={{
	position:"absolute",
	top:-22,
	right:0,
	left:218,
	marginBottom:40
}} >
	<Icon name="close-outline" size={25} color="black" />
</TouchableOpacity>
	  </View>
    </ModalContent>
  </Modal>
			</View>
        )
    }
}